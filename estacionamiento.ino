#include <SoftwareSerial.h>
SoftwareSerial BT1(0,1); // RX, TX
// definitions
//output
#define Motor 5
//input
#define Itr A1
#define InfraRed A0
#define MidPoint 500.0
#define RigidPoint 800.0
#define EmergencyButton A5

// const
#define Spaces 6

// variables 
int point = 0;
int moveTo = 0;
int inputs[2];
int movements = 0;
bool isEmergency = false;

// customer vars
int customerNumber = -1;
int customerLed[3] = {2,3,4};
int customerButton[3] = {A4,A3,A2};
bool customerState[3] ={LOW,LOW,LOW};

//set up
void setup() {
  // put your setup code here, to run once:
  pinMode(Motor, OUTPUT);

  for (int i = 0; i < 3; i++){
    pinMode(customerLed[i], OUTPUT);
    pinMode(customerButton[i], INPUT);
  }
  
  BT1.begin(9600);
}

// loop
void loop() {
  //wait for the next call of movement
  Comunicate();
  moveTo = inputs[0];
  customerNumber = inputs[1];

  // calculate the number cars to move
  if (moveTo < point){
      movements = Spaces - point + moveTo;
  }
  else{
      movements = moveTo - point;
  }
  // asign new position
  point = moveTo;
  
  BT1.println("movements: " + String(movements));
  // move the motor according to the number of movements
  for(int i = 0; i< movements; i++){
    Move();
  }
  BT1.println("current: " + String(point));

  // if its a customer
  if(customerNumber > -1){
    BT1.println("Its a customer");
    customerState[customerNumber] = customerState[customerNumber] == HIGH? LOW : HIGH;
    digitalWrite(customerLed[customerNumber], customerState[customerNumber]);
  }
  else{
    BT1.println("Its a Employee");
  }

  if(isEmergency){
    delay(500);
  }
  else{
    WaitForLoad();
  }
  
  BT1.println("dontwait");
}
