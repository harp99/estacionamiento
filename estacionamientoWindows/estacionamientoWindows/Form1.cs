﻿using OnBarcode.Barcode;
using System;
using System.Drawing;
using System.Drawing.Printing;
using System.IO.Ports;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;

namespace estacionamientoWindows
{
    public partial class Form1 : Form
    {
        /// <summary>
        /// class to manages the floors
        /// </summary>
        private class Floor
        {
            public string ocupiedBy { get; set; }
            public string customernumber { get; set; }
            public bool isCustomer { get { return !isEmployee(ocupiedBy); } }
        }

        Floor[] floors;
        private bool print = false;

        public Form1()
        {
            InitializeComponent();

            floors = new Floor[6];

            if (!port.IsOpen)
                try { port.Open(); }
                catch (Exception) { }
        }

        /// <summary>
        /// if tge entered user is an employee
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool isEmployee(string id)
        {
            return id == "13211410"
                || id == "13211428"
                || id == "13211427";
        }

        /// <summary>
        /// just to check  te connection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (!port.IsOpen)
                try { port.Open(); }
                catch (Exception excepton) { this.Text = excepton.Message.ToString(); }
        }

        /// <summary>
        /// convert the recived data to a string
        /// </summary>
        /// <param name="line"></param>
        private delegate void LineReceivedEvent(string line);
        private void LineReceived(string line)
        {
            //What to do with the received line here
            try
            {
                line = line.Split('\r')[0];
                line = line.Trim();
                if (line.Length > 1)
                {
                    string[] lines = line.Split('#');

                    if ("estacionamiento99".EndsWith(lines[0])
                        && lines.Length == 2)
                    {
                        label1.Text = lines[0] + "," + lines[1];
                        if (!floors.Where(floor => floor != null)
                            .Any(floor => floor.customernumber == lines[1]))
                        {
                            Floor customer = new Floor()
                            {
                                //ocupiedBy = Convert.ToBase64String(Encoding.UTF8.GetBytes(GenerateID())),
                                ocupiedBy = GenerateID().Replace("-", ""),
                                customernumber = lines[1]
                            };

                            //grab only the midle half
                            customer.ocupiedBy = customer.ocupiedBy.Substring(customer.ocupiedBy.Length / 4, customer.ocupiedBy.Length / 2);

                            // print barcode;
                            GenerateBacode(customer.ocupiedBy);

                            GetEmptySpace(customer);
                        }
                    }
                }
            }
            catch (Exception Ex) { }
        }

        /// <summary>
        /// method to generate an random enchypte temporal id
        /// reference: https://stackoverflow.com/questions/3652944/how-securely-unguessable-are-guids
        /// </summary>
        /// <returns></returns>
        private string GenerateID()
        {
            // Get 16 cryptographically random bytes
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            byte[] data = new byte[16];
            rng.GetBytes(data);

            // Mark it as a version 4 GUID
            data[7] = (byte)((data[7] | (byte)0x40) & (byte)0x4f);
            data[8] = (byte)((data[8] | (byte)0x80) & (byte)0xbf);

            return new Guid(data).ToString();
        }

        private void SendInformation(int point, bool remove)
        {
            string s = String.Format("estacionamiento99#{0}#{1}", point, floors[point].customernumber);


            if (!port.IsOpen)
                try { port.Open(); }
                catch (Exception) { }

            port.Write(s);

            if (remove)
                floors[point] = null;
        }

        /// <summary>
        /// if the port is resiving information
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void port_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            if (!port.IsOpen)
                try { port.Open(); }
                catch (Exception) { }

            string read = port.ReadExisting();

            this.BeginInvoke(new LineReceivedEvent(LineReceived), read);
        }

        // empty the floors
        private void reset()
        {
            floors = new Floor[6];
        }

        /// <summary>
        /// when the text is changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            // if its already registered 
            if (floors.Where(floor => floor != null).Any(floor => floor.ocupiedBy == textBox1.Text))
            {
                for (int i = 0; i < floors.Length; i++)
                {
                    if (floors[i] !=  null && floors[i].ocupiedBy == textBox1.Text)
                    {
                        SendInformation(i, true);
                        textBox1.Text = "";
                        break;
                    }
                }
            }
            else if (isEmployee(textBox1.Text))
            {
                Floor temp = new Floor()
                {
                    ocupiedBy = textBox1.Text,
                    customernumber = "-1"
                };

                GetEmptySpace(temp);
                textBox1.Text = "";
            }
        }

        /// <summary>
        /// code to generate barcode
        /// </summary>
        /// <param name="_data"></param>
        private void GenerateBacode(string _data)
        {
            Linear barcode = new Linear();
            barcode.Type = BarcodeType.CODE128;
            barcode.Data = _data;
            barcode.drawBarcode("GeneratedBacode.png");


            // print
            if (print)
            {
                PrintDoc();
            }
        }

        /// <summary>
        /// method to print a document
        /// reference: https://stackoverflow.com/questions/9982579/printing-image-with-printdocument-how-to-adjust-the-image-to-fit-paper-size
        /// </summary>
        private void PrintDoc()
        {
            PrintDocument pd = new PrintDocument();
            pd.DefaultPageSettings.PrinterSettings.PrinterName = "Canon MG3000 series";
            pd.DefaultPageSettings.Landscape = true; //or false!
            pd.PrintPage += (sender, args) =>
            {
                using (Image i = Image.FromFile("GeneratedBacode.png"))
                {
                    Rectangle m = args.MarginBounds;

                    if ((double)i.Width / (double)i.Height > (double)m.Width / (double)m.Height) // image is wider
                    {
                        m.Height = ((int)((double)i.Height / (double)i.Width * (double)m.Width)) / 2;
                        m.Width /= 2;
                    }
                    else
                    {
                        m.Width = ((int)((double)i.Width / (double)i.Height * (double)m.Height)) / 2;
                        m.Height /= 2;
                    }
                    args.Graphics.DrawImage(i, m);
                }
            };
            pd.Print();
            
        }

        /// <summary>
        /// method to send information on an empty space
        /// method to assign floor to and empty space in floors
        /// </summary>
        /// <param name="temp"></param>
        private void GetEmptySpace(Floor temp)
        {
            for (int i = 0; i < floors.Length; i++)
            {
                if (floors[i] == null &&
                        !floors.Where(floor => floor != null)
                            .Any(floor => floor.customernumber == temp.ocupiedBy))
                {
                    floors[i] = temp;
                    SendInformation(i, false);
                    break;
                }
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            floors = new Floor[6];
        }

        private void button2_Click(object sender, EventArgs e)
        {
            print = !print;
            button2.Text = print ? "printer on" : "printer off";
        }
    }
}
