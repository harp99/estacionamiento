// spin motor untill itr detects something
void Move(){
  digitalWrite(Motor, HIGH);

  double lectura = analogRead(Itr);
  while(lectura < MidPoint)
  {
    delay(100);
    lectura = analogRead(Itr);
  }
  while(lectura > MidPoint)
  {
    delay(100);
    lectura = analogRead(Itr);
  }
  
  digitalWrite(Motor, LOW);
}

