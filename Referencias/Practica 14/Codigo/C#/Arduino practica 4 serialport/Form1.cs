﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;

namespace Arduino_practica_4_serialport
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();



            if (!port.IsOpen)
                try { port.Open(); }
                catch (Exception) { }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            string s = "";
            foreach (char c in textBox1.Text)
                if (c <= '9' && c >= '0')
                    s += c;


            if (s.Length > 0)
            {
                int cut = (s.Length == 1) ? 1 : 2;
                textBox1.Text = s.Substring(0, cut);
            }
            else
                textBox1.Text = "";

            if (s.Length == 0)
                s = "0";
            else if (Int32.Parse(s) > 15)
                s = "15";

            s = "leds" + "#" + s;

            if (!port.IsOpen)
                try { port.Open(); }
                catch (Exception) { }
            else
            {

                //byte[] buffer = Encoding.ASCII.GetBytes(s);
                port.Write(s);
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (!port.IsOpen)
                try { port.Open(); }
                catch (Exception excepton) { this.Text = excepton.Message.ToString(); }
        }

        private void port_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            if (!port.IsOpen)
                try { port.Open(); }
                catch (Exception) { }

            string read = port.ReadExisting();
            if (read != label2.Text)
            this.BeginInvoke(new LineReceivedEvent(LineReceived), read);
        }

        private delegate void LineReceivedEvent(string line);
        private void LineReceived(string line)
        {
            //What to do with the received line here
            try
            {
                line = line.Trim();
                if (line.Length <= 2)
                {
                    Int32.Parse(line);
                    label2.Text = line;
                }
            }
            catch (Exception) { }
        }
    }
}
