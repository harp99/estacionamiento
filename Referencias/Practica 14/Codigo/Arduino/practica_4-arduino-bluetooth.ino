/*
 Name:    arduino.ino
 Created: 12/04/2017 4:25:13 PM
 Author:  irnh3
*/
#include <SoftwareSerial.h>
SoftwareSerial BT1(0,1); // RX, TX

int input[4] = { 2,3,4,5 };
bool bins[4];
int output[4] = { 10, 11, 12, 13 };
bool bins2[4];
int value = 0;
int valueTemp = 0;
char character;
String command[2];
int c = 0;

// the setup function runs once when you press reset or power the board
void setup() {
  pinMode(input[0], INPUT);
  pinMode(input[1], INPUT);
  pinMode(input[2], INPUT);
  pinMode(input[3], INPUT);

  pinMode(output[0], OUTPUT);
  pinMode(output[1], OUTPUT);
  pinMode(output[2], OUTPUT);
  pinMode(output[3], OUTPUT);

  BT1.begin(9600);
}

// the loop function runs over and over again until power down or reset
void loop() {
  // put your main code here, to run repeatedly:
    command[0] = "";
    command[1] = "";
    c = 0;
    while (BT1.available() > 0) {
      character = BT1.read();
      if (character == '#') {
        c++;
      }
      else {
        command[c].concat(character);
      }
      delay(10);
    }

    if (command[0] == "leds") {
      DecToBin(command[1].toInt());

      for (int i = 0; i < 4; i++) {
        digitalWrite(output[i], bins[i] == 1 ? HIGH : LOW);
      }
    }

    for (int i = 0; i < 4; i++)
    {
      bins2[i] = digitalRead(input[i]);
    }
    valueTemp = (int)BinToDec(bins2);
    if (value != valueTemp) {
      value = valueTemp;
      BT1.println(valueTemp);
    }
}

float BinToDec(bool bin[4]) {
  float val = 0;
  int ghostbit = 0;
  for (int i = 0; i < 4; i++) {
    val += pow(2, i) * bin[i];
    if (i > 1 && ghostbit < 1)
      ghostbit = bin[i];
  }
  val += ghostbit;
  return val;
}

void DecToBin(int Dec) {
  for (int i = 0; i < 4; i++) {
    bins[i] = (Dec % 2) > 0 ? HIGH : LOW;
    Dec = Dec / 2;
  }
}
