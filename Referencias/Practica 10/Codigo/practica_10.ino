int led = 13;
int resistor = 2;
bool flag = false;
int Output = LOW;
int val = 0;
int val2 = 0;

// the setup routine runs once when you press reset:
void setup() {
  // initialize the digital pin as an output.
  pinMode(led, OUTPUT);
  pinMode(resistor, INPUT);
  val = digitalRead(resistor);
  val2 = val;
  Serial.begin(9600);
}

// the loop routine runs over and over again forever:
void loop() {
  val2 = digitalRead(resistor);
  Serial.print(val2);
  if (val2 != val)
  {
    val = val2;
    if (flag)
    {
      if (Output == LOW)
      {
        Output = HIGH;
      }
      else
      {
        Output = LOW;
      }
      digitalWrite(led, Output);
      flag = !flag;
    }
    else
    {
      flag = !flag;
    }
  }
}
